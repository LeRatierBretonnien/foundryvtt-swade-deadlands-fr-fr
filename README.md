# FoundryVTT - SWADE-Deadlands-fr

[FRENCH ONLY - SORRY]

Ce module se base sur le système SWADE pour ajouter les compétences/atouts/handicaps/pouvoirs de Dealdands Reloaded.

## Installation

Pour l'installer, suivre ces instructions : 

1.  Au sein de Foundry, allez dans le menu "Configuration and Setup", puis sur l'onglet "Modules"
2.  Installez le module de françisation de l'interface principale : https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr/raw/master/fr-FR/module.json
3.  Installez le module 'SWADE'  : https://gitlab.com/florad-foundry/swade/-/raw/master/src/system.json
4.  Installez ce module : https://gitlab.com/LeRatierBretonnien/foundryvtt-swade-deadlands-fr-fr/-/raw/master/module.json


## Usage Instructions

Il n'y a pas de règles particulières, il faut juste positionner la langue à "Français" dans les réglages du système.

## Contribuer

Pour contribuer, vous pouvez me joindre sur le Discord de FoundryVTT ou sur Facebook Messenger, pseudo : LeRatierBretonnien

## Statut

Au 07/05/2020 : 

Les compétences, atouts (de base, d'arcanes et de déterrés), handicaps, pouvoirs.

## Evolutions

* Ajout de tout les pouvoirs (ie tout ceux listés dans l'écran épique).
* Modification de l'interface pour donner une ambiance plus "western"
* Ajouts des tables spécifiques (terreur, etc, eyc)
* Quelques automatisations à clarifier.

## Compatibility

Foundry : 0.5.5+
SWADE : 0.7+


## Acknowledgments

* Thanks to FloRad to its SWADE module

## License

SWADE-Deadlands-fr is a module for Foundry VTT by LeRatierBretonnien and is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).

This game references the Savage Worlds game system and Deadlands Reloaded, available from Pinnacle Entertainment Group at www.peginc.com. Savage Worlds, Deadlands Reloaded and all associated logos and trademarks are copyrights of Pinnacle Entertainment Group. Used with permission. Pinnacle makes no representation or warranty as to the quality, viability, or suitability for purpose of this product. 

La traduction Française de Deadands Reloaded est la propriété de Black-Book Editions.
