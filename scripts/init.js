/* Load system tables */
Hooks.once("init", () => {
    // load tables from system folder
    FilePicker.browse("data", "modules/SWADE-Deadlands-fr/tables").then(resp => {
      try 
      {
      if (resp.error)
        throw ""
      for (var file of resp.files)
      {
        try {
          if (!file.includes(".json"))
            continue
          let filename = file.substring(file.lastIndexOf("/")+1, file.indexOf(".json"));
          fetch(file).then(r=>r.json()).then(async records => {
            DEADLANDS_Tables[filename] = records;
          })
        }
        catch(error) {
         console.error("Error reading " + file + ": " + error)
        }
      }
    }
    catch
    {
      // Do nothing
    }
    })
} );
