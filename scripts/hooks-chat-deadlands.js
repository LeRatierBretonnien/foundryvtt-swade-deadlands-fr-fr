/* Hook chat message */
Hooks.on("chatMessage", (html, content, msg) => {
    // Setup new message's visibility
    let rollMode = game.settings.get("core", "rollMode");
    if (["gmroll", "blindroll"].includes(rollMode)) msg["whisper"] = ChatMessage.getWhisperIDs("GM");
    if (rollMode === "blindroll") msg["blind"] = true;
    msg["type"] = 0;

    // Split input into arguments
    let command = content.split(" ").map(function (item) {
        return item.trim();
    });

// Roll on a table
    if (command[0] === "/table") {
        // If no argument, display help menu
        if (command.length === 1)
            msg.content = DEADLANDS_Tables.formatChatRoll("menu");
        else {
            // [0]: /table [1]: <table-name> [2]: argument1 [3]: argument2
            let modifier, column; // Possible arguments
            // If argument 1 is a number use it as the modifier
            if (!isNaN(command[2])) {
                modifier = parseInt(command[2]);
                column = command[3]
            } else // if argument 1 is not a number, use it as column
            {
                modifier = parseInt(command[3]),
                    column = command[2]
            }
            // Call tables class to roll and return html
            msg.content = DEADLANDS_Tables.formatChatRoll(command[1], {modifier: modifier}, column)
        }
        // Create message and return false to not display user input of `/table`
        if (msg)
            ChatMessage.create(msg);
        return false;
    }
} );
