/**
 * This class handles all aspects of custom DEADLANDS tables.
 * 
 * The DEADLANDS_Tables is given table objects on 'init' and 'ready' hooks by
 * both the system, modules, and the world. See the tables folder for 
 * how they're structured. All files in that folder will be
 * added to DEADLANDS_Tables if possible. 
 */

class DEADLANDS_Tables
{

  /**
   * The base function to retrieve a result from a table given various parameters.
   * 
   * Options: 
   * `modifier` - modify the roll result by a certain amount
   * `minOne` - If true, the minimum roll on the table is 1 (used when a negative modifier is applied)
   * `lookup` - forego rolling and use this value to lookup the result on the table.
   * 
   * @param {String} table Table name - the filename of the table file
   * @param {Object} options Various options for rolling the table, like modifier
   * @param {String} column Which column to roll on, if possible.
   */
  static rollTable(table, options = {}, column = null)
  {
    let modifier = options.modifier || 0;
    let minOne = options.minOne || false;
    let maxSize = options.maxSize || false;

    table = table.toLowerCase();
    if (this[table])
    {
      let die = this[table].die;
      let tableSize;
      // Take the last result of the table, and find it's max range, that is the highest value on the table.
      if (!this[table].columns)
        tableSize = this[table].rows[this[table].rows.length - 1].range[1];
      else
      {
        tableSize = this[table].rows[this[table].rows.length - 1].range[this[table].columns[0]][1]; // This isn't confusing at all - take the first column, find its last (max) value, that is the table size
      }
      // If no die specified, just use the table size and roll
      if (!die)
        die = `1d${tableSize}`;
      let roll = new Roll(`${die} + @modifier`,{modifier}).roll();

      let rollValue = options.lookup || roll.total; // options.lookup will ignore the rolled value for the input value
      let displayTotal = options.lookup || roll.result; // Roll value displayed to the user
      if (modifier == 0)
        displayTotal = eval(displayTotal) // Clean up display value if modifier 0 (59 instead of 59 + 0)
      if (rollValue <= 0 && minOne) // Min one provides a lower bound of 1 on the result
        rollValue = 1;

      else if (rollValue <= 0)
        return {
          roll: rollValue
        };

      if (rollValue > tableSize)
        rollValue = tableSize;

      // Lookup the value on the table, merge it with the roll, and return
      return mergeObject(this._lookup(table, rollValue, column), ({roll: displayTotal}));
    }
    else
    {}
  }

  /**
   * Retrieves a value from a table, using the column if specified
   * 
   * @param {String} table table name
   * @param {Number} value value to lookup
   * @param {String} column column to look under, if needed
   */
  static _lookup(table, value, column = null)
  {
    if (!column)
      for (let row of this[table].rows)
      {
        if (value >= row.range[0] && value <= row.range[1])
          return row
      }
    else
    {
      for (let row of this[table].rows)
      {
        if (value >= row.range[column][0] && value <= row.range[column][1])
          return row
      }
    }
  }


  /* -------------------------------------------- */

  // Therefore, change specific locations to generalized ones (rarm -> arm)
  static generalizeTable(table)
  {
    table = table.toLowerCase();
    return table;
  }

  /* -------------------------------------------- */

  /**
   * 
   * Wrapper for rollTable to format rolls from chat commands nicely.
   * 
   * Calls rollTable() and displays the result in a specific format depending
   * on the table rolled on.
   * 
   * @param {String} table Table name - the filename of the table file
   * @param {Object} options Various options for rolling the table, like modifier
   * @param {String} column Which column to roll on, if possible.
   */
  static formatChatRoll(table, options = {}, column = null)
  {
    //console.log("Table requested :", table);
    table = this.generalizeTable(table);

    // If table has columns but none given, prompt for one.
    if (this[table] && this[table].columns && column == null)
    {
      return this.promptColumn(table, options);
    }

    let result = this.rollTable(table, options, column);
    if (options.lookup && !game.user.isGM) // If the player (not GM) rolled with a lookup value, display it so they can't be cheeky cheaters
      result.roll = "Lookup: " + result.roll;
    try
    {
      // Cancel the roll if below 1 and not minimum one
      if (result.roll <= 0 && !options.minOne)
        return `Roll: ${result.roll} - canceled`
    }
    catch
    {}
    
    return `<b>${this[table].name} - ${result.roll} </b><br>` + game.i18n.localize(result.description);
    
  }

  /**
   * Show the table help menu, display all tables as clickables and hidden tables if requested.
   * 
   * @param {Boolean} showHidden Show hidden tables
   */
  static tableMenu(showHidden = false)
  {
    let tableMenu = "<b><code>/table</code> Commandes </b><br>"
    let hiddenTableCounter = 0;

    // For each table, display a clickable link.
    for (let tableKey of Object.keys(this))
    {
      tableMenu += `<a data-table='${tableKey}' class='table-click'><i class="fas fa-list"></i> <code>${tableKey}</code></a> - ${this[tableKey].name}<br>`
    }
    return tableMenu;
  }


}
